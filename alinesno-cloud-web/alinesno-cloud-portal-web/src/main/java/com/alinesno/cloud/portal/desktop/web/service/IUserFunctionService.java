package com.alinesno.cloud.portal.desktop.web.service;

import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.core.services.IBaseService;
import com.alinesno.cloud.portal.desktop.web.entity.UserFunctionEntity;
import com.alinesno.cloud.portal.desktop.web.repository.UserFunctionRepository;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-21 19:03:36
 */
@NoRepositoryBean
public interface IUserFunctionService extends IBaseService<UserFunctionRepository, UserFunctionEntity, String> {

	boolean updateFunctions(String id, String rolesId);

	List<UserFunctionEntity> findAllByUid(String id);

}
