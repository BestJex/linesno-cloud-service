package com.alinesno.cloud.portal.desktop.web.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import com.alinesno.cloud.portal.desktop.web.entity.ModuleEntity;
import com.alinesno.cloud.portal.desktop.web.entity.UserFunctionEntity;
import com.alinesno.cloud.portal.desktop.web.entity.UserModuleEntity;
import com.alinesno.cloud.portal.desktop.web.repository.UserModuleRepository;
import com.alinesno.cloud.portal.desktop.web.service.IModuleService;
import com.alinesno.cloud.portal.desktop.web.service.IUserFunctionService;
import com.alinesno.cloud.portal.desktop.web.service.IUserModuleService;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-21 19:10:44
 */
@Service
public class UserModuleServiceImpl extends IBaseServiceImpl<UserModuleRepository, UserModuleEntity, String>
		implements IUserModuleService {

	// 日志记录
	private static final Logger log = LoggerFactory.getLogger(UserModuleServiceImpl.class);

	@Autowired
	private IModuleService moduleService ; 
	
	@Autowired
	private IUserFunctionService userFunctionService ; 

	@Transactional
	@Override
	public boolean updateModule(String id, String rolesId) {
		log.debug("id:{} , rolesId:{}", id, rolesId);

		// 删除所有用户功能
		jpa.deleteByUid(id);

		List<UserModuleEntity> fs = new ArrayList<UserModuleEntity>();

		String roleArr[] = rolesId.split(",");
		for (String r : roleArr) {
			Optional<ModuleEntity> lp = moduleService.findById(r);
			if (lp.isPresent()) {
				ModuleEntity l = lp.get();

				UserModuleEntity u = new UserModuleEntity();
				
				u.setModuleName(l.getModuleName());
				u.setModuleDesc(l.getModuleDesc());
				u.setModuleLogo(l.getModuleLogo());
				u.setModulePath(l.getModulePath());
				u.setOpenTarget(l.getOpenTarget());
				
				u.setMid(l.getId());
				u.setUid(id);

				fs.add(u);
			}
		}

		jpa.saveAll(fs);
		return true;
	}

	@Override
	public List<UserModuleEntity> findAllByUid(String id) {
		return jpa.findAllByUid(id) ;
	}

	@Override
	public List<UserModuleEntity> findOftenUse(String id) {
		Page<UserModuleEntity> pM = jpa.findAll(PageRequest.of(0, 5 , Sort.by(Sort.Direction.DESC, "clickCount"))) ;  // 查询前5条模块
		List<UserModuleEntity> mList = new ArrayList<UserModuleEntity>() ; 
		
		if(pM != null && pM.getContent() != null) {
			for(UserModuleEntity m : pM.getContent()) {
				m.setClickCount(m.getClickCount()==null?0:m.getClickCount());
				mList.add(m) ;
			}
		}
		
		Page<UserFunctionEntity> pU = userFunctionService.findAll(PageRequest.of(0, 5 , Sort.by(Sort.Direction.DESC, "clickCount"))) ;  // 查询前5条模块
		if(pU != null && pU.getContent() != null) {
			List<UserFunctionEntity> fs = pU.getContent() ; 
			for(UserFunctionEntity f : fs) {
				UserModuleEntity m = new UserModuleEntity() ; 
				
				m.setModuleDesc(f.getFunctionDesc());
				m.setModuleLogo(f.getFunctionLogo());
				m.setModuleName(f.getFunctionName());
				m.setModulePath(f.getFunctionPath());
				m.setClickCount(m.getClickCount()==null?0:m.getClickCount());
				
				log.debug("mList:{}" , mList);
				log.debug("m:{}" , m);
				
				mList.add(m) ;
			}
		}
	
		Collections.sort(mList,new Comparator<UserModuleEntity>() {
			@Override
			public int compare(UserModuleEntity o1, UserModuleEntity o2) {
				return o2.getClickCount() - o1.getClickCount();  //   升序
			}
		});
	
		return mList ;
	}

}
