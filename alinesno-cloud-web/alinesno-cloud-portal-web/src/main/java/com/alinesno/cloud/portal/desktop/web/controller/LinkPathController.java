package com.alinesno.cloud.portal.desktop.web.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.feign.dto.ManagerAccountDto;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.bean.DatatablesPageBean;
import com.alinesno.cloud.common.web.base.controller.LocalMethodBaseController;
import com.alinesno.cloud.common.web.base.response.ResponseBean;
import com.alinesno.cloud.common.web.base.response.ResponseGenerator;
import com.alinesno.cloud.common.web.login.session.CurrentAccountSession;
import com.alinesno.cloud.portal.desktop.web.entity.LinkPathEntity;
import com.alinesno.cloud.portal.desktop.web.service.ILinkPathService;
import com.alinesno.cloud.portal.desktop.web.service.IUserFunctionService;

/**
 * <p> 前端控制器 </p>
 *
 * @author LuoAnDong
 * @since 2019-05-18 14:46:04
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("portal/desktop/web/linkPath")
public class LinkPathController extends  LocalMethodBaseController<LinkPathEntity , ILinkPathService> {

	//日志记录
	private static final Logger log = LoggerFactory.getLogger(LinkPathController.class);

	@Autowired
	private IUserFunctionService userFunctionService ; 

	@TranslateCode(value="[{hasStatus:has_status}]")
	@ResponseBody
	@PostMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, feign , page) ;
    }

	@ResponseBody
	@GetMapping("/changeLinkDesign")
	public ResponseBean changeModuleDesign(Model model, String id) {
		log.debug("id:{}" , id);
		Optional<LinkPathEntity> parentModule = feign.findById(id) ; 
		if(parentModule.isPresent()) {
			LinkPathEntity m = parentModule.get() ; 
			int designInt = Integer.parseInt(m.getLinkDesign())+1 ; 
			
			log.debug("designInt:{} , newDesignInt:{}" , designInt , designInt%2);
			m.setLinkDesign(String.valueOf(designInt%2)) ; 
			feign.save(m) ;
		}
		return ResponseGenerator.genSuccessResult() ; 
	}

	@ResponseBody
	@PostMapping("/authAccount")
	public ResponseBean authAccount(Model model, String rolesId , HttpServletRequest request) {
		log.debug("rolesId:{}" , rolesId);
		ManagerAccountDto account = CurrentAccountSession.get(request) ; 

		boolean b = userFunctionService.updateFunctions(account.getId() , rolesId) ; 
		log.debug("update functions:{}" , b);
		
		return ResponseGenerator.genSuccessResult() ; 
	}
	
}


























