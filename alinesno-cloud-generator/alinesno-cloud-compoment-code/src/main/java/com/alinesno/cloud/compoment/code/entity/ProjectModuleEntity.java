package com.alinesno.cloud.compoment.code.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 项目选择模块
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Entity
@Table(name="project_module")
public class ProjectModuleEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 项目编码
     */
	private String projectCode;
    /**
     * 模块编码
     */
	private String moudleCode;


	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	public String getMoudleCode() {
		return moudleCode;
	}

	public void setMoudleCode(String moudleCode) {
		this.moudleCode = moudleCode;
	}


	@Override
	public String toString() {
		return "ProjectModuleEntity{" +
			"projectCode=" + projectCode +
			", moudleCode=" + moudleCode +
			"}";
	}
}
