package com.alinesno.cloud.compoment.code.devops.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.compoment.code.devops.GenerateCode;
import com.alinesno.cloud.compoment.code.devops.tools.WSTools;
import com.alinesno.cloud.compoment.code.entity.ProjectJobEntity;

/**
 * 用于代码生成业务
 * Created by lixin on 2018/2/1.
 */
@Component
@Service
public class GenerateCodeImpl implements GenerateCode {
	
    protected final static Logger logger = LoggerFactory.getLogger(GenerateCodeImpl.class);

    /**
     * 根据项目码创建项目代码
     *
     * @param projectCode 项目编码
     * @param projectJob  项目job
     * @param webSocket   socket连接
     */
    @Override
    public void aiCode(String projectCode, ProjectJobEntity projectJob, WSTools webSocket) {
//        String path = logsSV.createLogFiles(projectCode, projectJob.getCreateTime());
//        try {
//
//            //1.创建项目
//            String log = "Start By AI-Code @Copyright <a href='http://www.aicode.io' target='_blank'>AI-Code</a>";
//            webSocket.send(log);
//            logsSV.saveLogs(log, path);
//            Map<String, Object> map = Maps.newHashMap();
//            map.put("code", projectCode);
//            Project project = projectDAO.load(map);
//            String projectPath = this.buildProject(project, webSocket);
//            projectDAO.update(projectCode, project.getBuildNumber() != null ? project.getBuildNumber() + 1 : 1);
//            log = "已初始化项目 【 " + project.getName() + " ( " + project.getEnglishName() + " )】 工作空间";
//            webSocket.send(log);
//            logsSV.saveLogs(log, path);
//            projectJobLogsDAO.insert(new ProjectJobLogs(projectJob.getCode(), log));
//            logger.info("创建工作空间库完成");
//            logsSV.saveLogs("创建工作空间库完成", path);
//
//
//            //2.获取类信息
//            log = "转化数据库结构与类模型...";
//            webSocket.send(log);
//            logsSV.saveLogs(log, path);
//            List<ProjectMap> projectMapList = project.getProjectMapList();
//            List<MapClassTable> mapClassTableList = new ArrayList<>();
//            projectMapList.forEach(projectMap -> {
//                mapClassTableList.add(projectMap.getMapClassTable());
//            });
//            webSocket.send("转化数据库结构与类模型成功！");
//            logsSV.saveLogs("转化数据库结构与类模型成功！", path);
//
//            //3.获取模板信息
//            List<ProjectFramwork> projectFramworkList = project.getProjectFramworkList();
//            //从git中检出技术模板库
//            log = ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>";
//            webSocket.send(log);
//            webSocket.send("开始 下载技术模板");
//            webSocket.send(log);
//            logsSV.saveLogs(log, path);
//            logsSV.saveLogs("开始 下载技术模板", path);
//            logsSV.saveLogs(log, path);
//            this.readyframeworksTemplateList(projectFramworkList, webSocket, path);
//            webSocket.send(log);
//            webSocket.send("结束 下载技术模板成功");
//            webSocket.send(log);
//            logsSV.saveLogs(log, path);
//            logsSV.saveLogs("结束 下载技术模板成功", path);
//            logsSV.saveLogs(log, path);
//
//            //4.生成源码
//            webSocket.send("开始生成源码...");
//            logsSV.saveLogs("开始生成源码...", path);
//            projectFramworkList.forEach(projectFramwork -> {
//                webSocket.send("已获取项目 【" + projectFramwork.getFrameworks().getName() + "】 的模板");
//                logsSV.saveLogs("已获取项目 【" + projectFramwork.getFrameworks().getName() + "】 的模板", path);
//                Map<String, Object> param = new HashMap<>();
//                param.put("frameworkCode", projectFramwork.getFrameworks().getCode());
//                Frameworks frameworks = projectFramwork.getFrameworks();
//                List<FrameworksTemplate> frameworksTemplateList = frameworksTemplateDAO.query(param);
//                frameworksTemplateList.forEach(frameworksTemplate -> {
//                    projectMapList.forEach(projectMap -> {
//                        this.generator(projectPath, project, frameworks, projectMap.getMapClassTable(), frameworksTemplate, mapClassTableList, webSocket);
//                    });
//                    webSocket.send("[生成] 模板 " + frameworksTemplate.getPath() + " 的源码");
//                    logsSV.saveLogs("[生成] 模板 " + frameworksTemplate.getPath() + " 的源码", path);
//                });
//            });
//
//            //清理临时模板数据
//            this.cleanTemplates(projectFramworkList);
//
//
//            //生成sql脚本到项目下
//            String sql = this.generateTsql(projectPath, project.getEnglishName(), projectCode);
//            webSocket.send("分布式唯一算法sql " + sql);
//            logsSV.saveLogs("分布式唯一算法sql " + sql, path);
//            String sqllog = "【已经生成】 " + project.getEnglishName() + "Sql 脚本文件并追加系统配置";
//            webSocket.send(sqllog);
//            logsSV.saveLogs(sqllog, path);
//            projectJobLogsDAO.insert(new ProjectJobLogs(projectJob.getCode(), sqllog));
//
//            //5.获取模块信息 TODO
//
//            //6.获取版本控制管理信息
//            map.clear();
//            map.put("projectCode", project.getCode());
//            ProjectRepositoryAccount projectRepositoryAccount = projectRepositoryAccountDAO.load(map);
//            if (projectRepositoryAccount != null) {
//                String gitLog = "获取代码仓库信息: " + projectRepositoryAccount.getAccount();
//                webSocket.send(gitLog);
//                logsSV.saveLogs(gitLog, path);
//                projectJobLogsDAO.insert(new ProjectJobLogs(projectJob.getCode(), gitLog));
//                GitTools.commitAndPush(new File(projectPath), projectRepositoryAccount.getAccount(), projectRepositoryAccount.getPassword(), "AI-Code 为您构建代码，享受智慧生活");
//                gitLog = "代码已经提交到 ⇛⇛⇛ <a style='text-decoration:underline;' href='" + projectRepositoryAccount.getHome() + "' target='_blank'>[" + projectRepositoryAccount.getHome() + "] </a>仓库";
//                webSocket.send(gitLog);
//                logsSV.saveLogs(gitLog, path);
//                projectJobLogsDAO.insert(new ProjectJobLogs(projectJob.getCode(), gitLog));
//            }
//
//            //7.创建压缩文件
//            this.zipProject(project);
//            String endLog = "代码已打包ZIP, ⇛⇛⇛  <a style='text-decoration:underline;' href='" + project.getDownloadUrl() + "' target='_blank'>[点击下载" + project.getEnglishName() + ".zip]</a>";
//            webSocket.send(endLog);
//            logsSV.saveLogs(endLog, path);
//            //记录日志
//            projectJobLogsDAO.insert(new ProjectJobLogs(projectJob.getCode(), endLog));
//            projectJob.setState(ProjectJob.State.Completed.name());
//            projectJobDAO.update(projectJob);
//        } catch (Exception e) {
//            e.printStackTrace();
//            logger.error(e.getMessage());
//            webSocket.send(e.getMessage());
//            logsSV.saveLogs(e.getMessage(), path);
//            projectJobLogsDAO.insert(new ProjectJobLogs(projectJob.getCode(), "ERROR : " + e.getMessage()));
//            projectJob.setState(ProjectJob.State.Error.name());
//            projectJobDAO.update(projectJob);
//        } finally {
//            Map<String, Object> map = Maps.newHashMap();
//            map.put("code", projectJob.getCode());
//            ProjectJob projectJobLoad = projectJobDAO.load(map);
//            if (projectJobLoad.getState().equals(ProjectJob.State.Completed.name())) {
//                ProjectJobLogs projectJobLogs = new ProjectJobLogs();
//                projectJobLogs.setCode(projectJob.getCode());
//                projectJobLogs.setLog("Finished: SUCCESS");
//                webSocket.send("Finished: SUCCESS");
//                logsSV.saveLogs("Finished: SUCCESS", path);
//                projectJobLogsDAO.insert(projectJobLogs);
//            } else {
//                ProjectJobLogs projectJobLogs = new ProjectJobLogs();
//                projectJobLogs.setCode(projectJob.getCode());
//                projectJobLogs.setLog("Finished: ERROR");
//                webSocket.send("Finished: ERROR");
//                logsSV.saveLogs("Finished: ERROR", path);
//                projectJobLogsDAO.insert(projectJobLogs);
//            }
//            ProjectJobLogs projectJobLogs = new ProjectJobLogs();
//            projectJobLogs.setCode(projectJob.getCode());
//            projectJobLogs.setLog("End");
//            webSocket.send("End");
//            
//            projectJobLogsDAO.insert(projectJobLogs);
//        }
    }

}
