package com.alinesno.cloud.compoment.code.repository;

import com.alinesno.cloud.common.core.orm.repository.IBaseJpaRepository;
import com.alinesno.cloud.compoment.code.entity.ProjectCodeCatalogEntity;

/**
 * <p>
  * 生成源码资料 持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
public interface ProjectCodeCatalogRepository extends IBaseJpaRepository<ProjectCodeCatalogEntity, String> {

}
