package com.alinesno.cloud.compoment.code.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.core.services.IBaseService;
import com.alinesno.cloud.compoment.code.entity.ProjectMapEntity;
import com.alinesno.cloud.compoment.code.repository.ProjectMapRepository;

/**
 * <p> 项目数据表 服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@NoRepositoryBean
public interface IProjectMapService extends IBaseService<ProjectMapRepository, ProjectMapEntity, String> {

}
