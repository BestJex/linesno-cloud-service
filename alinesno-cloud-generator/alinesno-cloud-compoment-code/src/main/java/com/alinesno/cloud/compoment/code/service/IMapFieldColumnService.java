package com.alinesno.cloud.compoment.code.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.core.services.IBaseService;
import com.alinesno.cloud.compoment.code.entity.MapFieldColumnEntity;
import com.alinesno.cloud.compoment.code.repository.MapFieldColumnRepository;

/**
 * <p> 字段属性映射信息 服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@NoRepositoryBean
public interface IMapFieldColumnService extends IBaseService<MapFieldColumnRepository, MapFieldColumnEntity, String> {

}
