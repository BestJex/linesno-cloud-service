package com.alinesno.cloud.compoment.code.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import com.alinesno.cloud.compoment.code.entity.FrameworksTemplateEntity;
import com.alinesno.cloud.compoment.code.repository.FrameworksTemplateRepository;
import com.alinesno.cloud.compoment.code.service.IFrameworksTemplateService;

/**
 * <p> 框架配置文件模板 服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Service
public class FrameworksTemplateServiceImpl extends IBaseServiceImpl<FrameworksTemplateRepository, FrameworksTemplateEntity, String> implements IFrameworksTemplateService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(FrameworksTemplateServiceImpl.class);

}
