package com.alinesno.cloud.common.web.base.advice.plugins;

import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alinesno.cloud.base.boot.feign.dto.ManagerApplicationDto;
import com.alinesno.cloud.base.boot.feign.facade.ManagerApplicationFeigin;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.advice.TranslatePlugin;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * 应用代码转换插件
 * @author LuoAnDong
 * @since 2019年4月7日 下午2:23:50
 */
@Component
public class ApplicationPlugin implements TranslatePlugin {

	@Autowired
	private ManagerApplicationFeigin managerApplicationFeigin ; 
	
	@Override
	public void translate(ObjectNode node, TranslateCode convertCode) {

		JsonNode jsonNode = node.get(APPLICATION_ID) ; 
		String applicationName = "未知应用" ; 
		if(jsonNode != null) {
			String id = jsonNode.asText() ; 
			log.debug("applicationId:{}" , id);
			if(StringUtils.isNotBlank(id)) {
				Optional<ManagerApplicationDto> dto = managerApplicationFeigin.findById(id) ; 
				if(dto.isPresent()) {
					applicationName = dto.get().getApplicationName() ;
				}
			}
		}
		node.put(APPLICATION_ID + LABEL_SUFFER, applicationName) ; 
	}

}
