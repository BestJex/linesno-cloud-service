package com.alinesno.cloud.common.web.base.thymeleaf;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.model.IModel;
import org.thymeleaf.model.IModelFactory;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractElementTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.spring5.context.SpringContextUtils;
import org.thymeleaf.templatemode.TemplateMode;

import com.alinesno.cloud.base.boot.feign.dto.ManagerCodeDto;
import com.alinesno.cloud.base.boot.feign.facade.ManagerCodeFeigin;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;

/**
 * 字典标签
 * @author LuoAnDong
 * @since 2019年1月31日 下午3:29:11
 */
public class DictionaryTagProcessor extends AbstractElementTagProcessor {
	
	private static final Logger log = LoggerFactory.getLogger(DictionaryTagProcessor.class) ; 

	private static final String TAG_NAME = "dictionary";
	private static final int PRECEDENCE = 1000;// 优先级

	public DictionaryTagProcessor(final String dialectPrefix) {
		super(TemplateMode.HTML, // 此处理器将仅应用于HTML模式
				dialectPrefix, // 要应用于名称的匹配前缀
				TAG_NAME, // 标签名称：匹配此名称的特定标签
				true, // 没有应用于标签名的前缀
				null, // 无属性名称：将通过标签名称匹配
				false, // 没有要应用于属性名称的前缀
				PRECEDENCE// 优先(内部方言自己的优先
		);
	}

	/**
	 * 标签内容解析
	 */
	@Override
	protected void doProcess(final ITemplateContext context, final IProcessableElementTag tag, final IElementTagStructureHandler structureHandler) {

		ApplicationContext ctx = SpringContextUtils.getApplicationContext(context);
		
		final IModelFactory modelFactory = context.getModelFactory();
		final IModel model = modelFactory.createModel();
		ManagerCodeFeigin managerCodeFeigin = ctx.getBean(ManagerCodeFeigin.class) ; 
		
		final String type = tag.getAttributeValue("type"); // 类型
		final String value = tag.getAttributeValue("value"); // 值(默认值) 
		final String name = tag.getAttributeValue("name"); // 名称
		
		log.debug("type = {} , feigin = {}" , type , managerCodeFeigin);
		
		StringBuffer sb = new StringBuffer() ; 
		if(StringUtils.isNotBlank(type)) {
			
			RestWrapper restWrapper = new RestWrapper() ; 
			restWrapper.eq("codeTypeValue", type)
					   .eq("hasStatus", "0") ; 
			List<ManagerCodeDto> list = managerCodeFeigin.findAll(restWrapper) ; 
			
			sb.append("<select class='form-control' id='"+name+"' name='"+name+"'>"); 
			
			for(ManagerCodeDto m : list) {
				if(StringUtils.isNotBlank(value) && value.equals(m.getCodeValue())) {
					sb.append("<option value='"+m.getCodeValue()+"' selected>"+m.getCodeName()+"</option>") ; 
				} else {
					sb.append("<option value='"+m.getCodeValue()+"'>"+m.getCodeName()+"</option>") ; 
				}
			}
			
			sb.append("</select>") ; 
		}
		
		model.add(modelFactory.createText(sb));
		structureHandler.replaceWith(model, false);
	}
}