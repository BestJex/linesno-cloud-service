package com.alinesno.cloud.common.web.base.controller;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alinesno.cloud.common.web.login.aop.AccountRecord;
import com.alinesno.cloud.common.web.login.aop.AccountRecord.RecordType;

/**
 * 跳转控制器
 * 
 * @author LuoAnDoong
 * @date 2017年9月7日
 */
@Controller
@Scope("prototype")
public class PageJumpController extends BaseController implements ErrorController {

	private static final Logger log = LoggerFactory.getLogger(PageJumpController.class);

	@Override
    public String getErrorPath() {
        return "/error";
    }

	/**
	 * 统一错误页面处理
	 * @param request
	 * @return
	 */
	@AccountRecord(value="错误页面",type=RecordType.ACCESS_PAGE)
    @RequestMapping("/error")
    public String handleError(HttpServletRequest request){
        //获取statusCode:401,404,500
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        log.debug("error status code = {}" , statusCode);
        
        if(statusCode == 401){
            return "/error/401";
        }else if(statusCode == 404){
            return "/error/404";
        }else if(statusCode == 403){
            return "/error/403";
        }else{
            return "/error/500";
        }
 
    }

	/**
	 * 两级页面跳转
	 * 
	 * @param packages
	 *            子包
	 * @param page
	 *            页面
	 * @return 返回jsp页面路径
	 */
	@RequestMapping(value = "/pages/{page}")
	public String viewOne(@PathVariable String page) {
		log.debug("page = {}", page );
		return page;
	}

	
	/**
	 * 两级页面跳转
	 * 
	 * @param packages
	 *            子包
	 * @param page
	 *            页面
	 * @return 返回jsp页面路径
	 */
	@RequestMapping(value = "/pages/{packages}/{page}")
	public String pages(@PathVariable String packages, @PathVariable String page) {
		log.debug("package = {} , page = {}", packages, page);
		return packages + "/" + page;
	}

	/**
	 * 三级页面路径
	 * 
	 * @param packages
	 *            父包
	 * @param children
	 *            子包
	 * @param page
	 *            页面
	 * @return 返回jsp页面路径
	 */
	@RequestMapping(value = "/pages/{packages}/{children}/{page}")
	public String viewChir(@PathVariable String packages, @PathVariable String children, @PathVariable String page) {
		log.debug("package = {} , children = {} ,  page = {}", packages, children, page);
		return packages + "/" + children + "/" + page;
	}

	/**
	 * 四级页面路径
	 * 
	 * @param packages
	 *            父包
	 * @param children
	 *            子包
	 * @param childrenFour
	 *            三级
	 * @param page
	 *            页面
	 * @return 返回jsp页面路径
	 */
	@RequestMapping(value = "/pages/{packages}/{children}/{childrenFour}/{page}")
	public String viewFour(@PathVariable String packages, @PathVariable String children,
			@PathVariable String childrenFour, @PathVariable String page) {
		log.debug("package = {} , children = {} ,  page = {}", packages, children, childrenFour, page);
		return packages + "/" + children + "/" + childrenFour + "/" + page;
	}
	
//	/**
//	 * 两级页面跳转
//	 * 
//	 * @param packages
//	 *            子包
//	 * @param page
//	 *            页面
//	 * @return 返回jsp页面路径
//	 */
//	@RequestMapping(value = "/static/{page}")
//	public String staticViewOne(@PathVariable String page) {
//		log.debug("page = {}", page );
//		return page;
//	}
//	
//	/**
//	 * 两级页面跳转
//	 * 
//	 * @param packages
//	 *            子包
//	 * @param page
//	 *            页面
//	 * @return 返回jsp页面路径
//	 */
//	@RequestMapping(value = "/static/{packages}/{page}")
//	public String staticPages(@PathVariable String packages, @PathVariable String page) {
//		log.debug("package = {} , page = {}", packages, page);
//		return packages + "/" + page;
//	}
//
//	/**
//	 * 三级页面路径
//	 * 
//	 * @param packages
//	 *            父包
//	 * @param children
//	 *            子包
//	 * @param page
//	 *            页面
//	 * @return 返回jsp页面路径
//	 */
//	@RequestMapping(value = "/static/{packages}/{children}/{page}")
//	public String staticViewChir(@PathVariable String packages, @PathVariable String children, @PathVariable String page) {
//		log.debug("package = {} , children = {} ,  page = {}", packages, children, page);
//		return packages + "/" + children + "/" + page;
//	}
//
//	/**
//	 * 四级页面路径
//	 * 
//	 * @param packages
//	 *            父包
//	 * @param children
//	 *            子包
//	 * @param childrenFour
//	 *            三级
//	 * @param page
//	 *            页面
//	 * @return 返回jsp页面路径
//	 */
//	@RequestMapping(value = "/static/{packages}/{children}/{childrenFour}/{page}")
//	public String staticViewFour(@PathVariable String packages, @PathVariable String children,
//			@PathVariable String childrenFour, @PathVariable String page) {
//		log.debug("package = {} , children = {} ,  page = {}", packages, children, childrenFour, page);
//		return packages + "/" + children + "/" + childrenFour + "/" + page;
//	}

}