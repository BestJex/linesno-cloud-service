package com.alinesno.cloud.common.core.auto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

import com.alinesno.cloud.common.core.context.ApplicationContextProvider;

/**
 * 引入自动类
 * @author LuoAnDong
 * @sine 2019年4月5日 下午3:34:07
 */
public class CommonCoreConfigurationSelector implements ImportSelector {

	@Override
	public String[] selectImports(AnnotationMetadata importingClassMetadata) {
		List<String> importBean = new ArrayList<String>() ; 

		importBean.add(ApplicationContextProvider.class.getName()) ; 
		
		return importBean.toArray(new String[] {}) ;
	}

	
}
