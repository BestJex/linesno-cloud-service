package com.alinesno.cloud.platform.stack.repository;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringRunner;

import com.alinesno.cloud.operation.cmdb.entity.UserEntity;
import com.alinesno.cloud.operation.cmdb.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest {

	private final Logger logger = LoggerFactory.getLogger(this.getClass()) ; 

	@Autowired
	private UserRepository userRepository ; 

	String phone = "15578942583" ; 
	String masterCode = "10608" ; 
	
	@Test
	public void testFindByFieldProp() {
		Page<UserEntity> list = userRepository.findByFieldProp("02", PageRequest.of(0, 10 , Sort.by(Direction.DESC, "addTime"))) ; 
		logger.debug("list= {}" , list);
	}
	
	@SuppressWarnings("serial")
	@Test
	public void testFindAllByMasterCodeAndPageable() {
		Specification<UserEntity> p = new Specification<UserEntity>() {
			@Override
			public Predicate toPredicate(Root<UserEntity> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				return null;
			}
        };
		Page<UserEntity> list = userRepository.findAll(p , PageRequest.of(0 , 20)) ; 
		logger.debug("list= {}" , list);
	}
	
	@Test
	public void testFindAllByMasterCode() {
		List<UserEntity> list = userRepository.findAllByMasterCode(masterCode) ; 
		logger.debug("list= {}" , list.size());
	}
	
	
	@Test
	public void testFindByPhone() {
		UserEntity user = userRepository.findByPhone(phone) ; 
		logger.debug("user = {}" , user);
	}
	
	@Test
	public void testSave() {
		UserEntity user = new UserEntity() ; 
		user.setName("罗小东");
		user.setPhone(phone); 
		user.setEmail("luoandon@gmail.com");
		user.setMasterCode("1001");
		user.setAddTime(new Timestamp(System.currentTimeMillis()));
		
		user = userRepository.save(user) ; 
		
		logger.debug("user = {}" , user);
	}

}
