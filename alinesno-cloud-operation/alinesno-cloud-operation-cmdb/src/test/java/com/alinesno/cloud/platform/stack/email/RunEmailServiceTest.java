package com.alinesno.cloud.platform.stack.email;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alinesno.cloud.operation.cmdb.third.email.RunEmailService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RunEmailServiceTest {

	@SuppressWarnings("unused")
	private final Logger logger = LoggerFactory.getLogger(this.getClass()) ; 

	@Autowired
	private RunEmailService runEmailService; 
	
	@Test
	public void testSendRunmanEmail() throws IOException {
		runEmailService.sendRunmanEmail(); 
	}

}
