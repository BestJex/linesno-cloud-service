package com.alinesno.cloud.operation.cmdb.service.impl;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.operation.cmdb.common.constants.OrderStatusEnum;
import com.alinesno.cloud.operation.cmdb.entity.OrderRecordEntity;
import com.alinesno.cloud.operation.cmdb.repository.OrderRecordRepository;
import com.alinesno.cloud.operation.cmdb.service.OrderRecordService;

/**
 * 订单记录服务
 * @author LuoAnDong
 * @since 2018年8月24日 上午7:36:40
 */
@Service
public class OrderRecordServiceImp implements OrderRecordService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private OrderRecordRepository orderRecordRepository; // 订单操作记录
	
	@Override
	public void saveRecord(String orderId, String recordCode, String actionMan) {
		this.saveRecord(orderId, recordCode, actionMan, null); 
	}

	@Async("processExecutor")
	@Override
	public void saveRecord(String orderId, String recordCode, String userId , String detail) {
		logger.debug("记录订单:{} , 状态:{} , 操作人员:{}, 详情:{}" , orderId , recordCode , userId , detail);
		try {
			
			OrderRecordEntity record = new OrderRecordEntity(orderId ,recordCode , OrderStatusEnum.getStatus(recordCode).getText(), userId) ; 
			record.setAddTime(new Date());
			if(StringUtils.isNoneBlank(detail)) {
				record.setActionDetail(detail); 
			}
			orderRecordRepository.save(record) ;
		} catch (Exception e) {
			logger.error("记录订单:{} 错误:{}" , orderId , e);
		} 
	}

}
