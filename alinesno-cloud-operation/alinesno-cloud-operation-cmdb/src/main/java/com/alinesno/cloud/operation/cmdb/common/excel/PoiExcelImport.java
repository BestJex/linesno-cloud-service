package com.alinesno.cloud.operation.cmdb.common.excel;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;

/**
 * excel 导入对象
 * 
 * @author LuoAnDong
 * @since 2019年5月8日 下午8:41:14
 */
public class PoiExcelImport {

	private static final Logger logger = LoggerFactory.getLogger(PoiExcelImport.class) ;
	
	public static <T> List<T> importBean(String path , Class<T> t) {
		
		ImportParams params = new ImportParams();
		long start = new Date().getTime();
		List<T> list = ExcelImportUtil.importExcel(new File(path), t.getClass() , params);
		
		logger.debug("使用时间:{}" , new Date().getTime() - start);
		
		return list ; 
	}
}
