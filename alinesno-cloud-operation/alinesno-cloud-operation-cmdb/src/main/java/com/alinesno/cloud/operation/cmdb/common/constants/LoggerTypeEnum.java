package com.alinesno.cloud.operation.cmdb.common.constants;

/**
 * 订单状态
 * 
 * @author LuoAnDong
 * @since 2018年8月5日 下午6:41:23
 */
public enum LoggerTypeEnum {

	LOGIN("login" , "登陆"), // 待接单
	LOGOUT("logout" , "退出"); // 已接单
	
	private String code;
	private String text ;
	
	LoggerTypeEnum(String code , String text) {
		this.code = code;
		this.text = text ; 
	}
	
	public String getText() {
		return this.text ; 
	}

	public String getCode() {
		return this.code;
	}
}