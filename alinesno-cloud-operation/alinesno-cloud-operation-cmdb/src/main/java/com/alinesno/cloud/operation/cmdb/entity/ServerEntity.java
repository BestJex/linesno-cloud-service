package com.alinesno.cloud.operation.cmdb.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 当前业务服务实体对象 
 * @author LuoAnDong
 * @since 2018年8月5日 下午1:34:14
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_server")
public class ServerEntity extends BaseEntity {

	private String serverCode ; // 物理服务器IP
	private String serverName ; // 物物理服务器IP
	private String serverDesc ; // 物理服务器描述 
	private int serverOrder ; // 物理服务器顺序
	
	public String getServerCode() {
		return serverCode;
	}
	public void setServerCode(String serverCode) {
		this.serverCode = serverCode;
	}
	public String getServerName() {
		return serverName;
	}
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	public String getServerDesc() {
		return serverDesc;
	}
	public void setServerDesc(String serverDesc) {
		this.serverDesc = serverDesc;
	}
	public int getServerOrder() {
		return serverOrder;
	}
	public void setServerOrder(int serverOrder) {
		this.serverOrder = serverOrder;
	}
//	public String getStatus() {
//		return status;
//	}
//	public void setStatus(String status) {
//		this.status = status;
//	}
	
}
