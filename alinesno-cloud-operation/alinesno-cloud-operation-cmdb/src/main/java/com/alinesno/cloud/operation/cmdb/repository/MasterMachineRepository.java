package com.alinesno.cloud.operation.cmdb.repository;
import java.util.List;

import com.alinesno.cloud.operation.cmdb.entity.MasterMachineEntity;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface MasterMachineRepository extends BaseJpaRepository<MasterMachineEntity, String> {

	MasterMachineEntity findByMasterCode(String masterCode);

	List<MasterMachineEntity> findAllByMasterCode(String masterCode);

	Iterable<MasterMachineEntity> findByMasterCodeNotInOrderByMasterOrderDesc(List<String> notIn);

}