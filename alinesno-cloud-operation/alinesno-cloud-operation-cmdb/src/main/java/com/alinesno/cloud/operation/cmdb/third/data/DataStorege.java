package com.alinesno.cloud.operation.cmdb.third.data;

/**
 * 数据存储接口
 * @author LuoAnDong
 * @since 2018年2月23日 下午2:33:45
 */
public interface DataStorege {

	/**
	 * 文件上传
	 * @param fileLoalAbcPath 文件本地绝对路径
	 * @return
	 */
	public String uploadData(String fileLoalAbcPath); 

	/**
	 * 获取文件下载链接
	 * @param fileId 文件保存id
	 * @return
	 */
	public String downloadData(String fileId); 

	/**
	 * 删除文件 
	 * @param fileId
	 * @return
	 */
	public boolean deleteData(String fileId); 
	
}
