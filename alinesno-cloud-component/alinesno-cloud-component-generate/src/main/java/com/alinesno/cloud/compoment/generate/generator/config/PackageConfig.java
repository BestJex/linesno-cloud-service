/**
 * Copyright (c) 2011-2020, hubin (jobob@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.alinesno.cloud.compoment.generate.generator.config;

import com.alinesno.cloud.compoment.generate.generator.StringUtils;

/**
 * 跟包相关的配置项
 * @author LuoAnDong
 * @since 2018年12月2日 上午10:45:16
 */
public class PackageConfig {

    /**
     * 父包名。如果为空，将下面子包名必须写全部， 否则就只需写子包名
     */
    private String parent = "com.baomidou";

    /**
     * 父包模块名。
     */
    private String moduleName = null;

    /**
     * Entity包名
     */
    private String entity = "entity";

    /**
     * Service包名
     */
    private String service = "service";

    /**
     * Service Impl包名
     */
    private String serviceImpl = "service.impl";
    /**
     * Mapper包名
     */
    private String mapper = "mapper";

    /**
     * Mapper XML包名
     */
    private String xml = "mapper.xml";

    /**
     * Controller包名
     */
    private String controller = "web";
   
    /**
     * Repository路径 
     */
    private String repository = "repository" ; 
    
    /**
     * RestController路径 
     */
    private String restController = "rest" ; 
    
    /**
     * Feign 路径 
     */
    private String feigin = "feign.facade" ; 
    
    /**
     * Feigin Dto路径
     */
    private String feiginDto = "feign.dto" ; 
   
    /**
     * jsp页面路径包名
     */
    private String page = "page" ; 
    
    /**
     * springboot启动类 
     */
    private String boot = "boot" ; 
   
    // 生成 yml 和 pom.xml 文件 
    private String springApplicationName ; 
    private String serverPort ; 
    private String databaseDriverClass ; 
    private String databaseUrl ; 
    private String databaseUsername ; 
    private String databasePassword ; 
    
    public String getAbcPath() {
		return abcPath;
	}

	public void setAbcPath(String abcPath) {
		this.abcPath = abcPath;
	}

	private String abcPath ;  
    private String articleId ; 
    private String groupId ; 

    public String getSpringApplicationName() {
		return springApplicationName;
	}

	public void setSpringApplicationName(String springApplicationName) {
		this.springApplicationName = springApplicationName;
	}

	public String getServerPort() {
		return serverPort;
	}

	public void setServerPort(String serverPort) {
		this.serverPort = serverPort;
	}

	public String getDatabaseDriverClass() {
		return databaseDriverClass;
	}

	public void setDatabaseDriverClass(String databaseDriverClass) {
		this.databaseDriverClass = databaseDriverClass;
	}

	public String getDatabaseUrl() {
		return databaseUrl;
	}

	public void setDatabaseUrl(String databaseUrl) {
		this.databaseUrl = databaseUrl;
	}

	public String getDatabaseUsername() {
		return databaseUsername;
	}

	public void setDatabaseUsername(String databaseUsername) {
		this.databaseUsername = databaseUsername;
	}

	public String getDatabasePassword() {
		return databasePassword;
	}

	public void setDatabasePassword(String databasePassword) {
		this.databasePassword = databasePassword;
	}

	public String getArticleId() {
		return articleId;
	}

	public void setArticleId(String articleId) {
		this.articleId = articleId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getParent() {
        if (StringUtils.isNotEmpty(moduleName)) {
            return parent + "." + moduleName;
        }
        return parent;
    }

    public PackageConfig setParent(String parent) {
        this.parent = parent;
        return this;
    }

    public String getModuleName() {
        return moduleName;
    }

    public PackageConfig setModuleName(String moduleName) {
        this.moduleName = moduleName;
        return this;
    }

    public String getEntity() {
        return entity;
    }

    public PackageConfig setEntity(String entity) {
        this.entity = entity;
        return this;
    }

    public String getService() {
        return service;
    }

    public PackageConfig setService(String service) {
        this.service = service;
        return this;
    }

    public String getServiceImpl() {
        return serviceImpl;
    }

    public PackageConfig setServiceImpl(String serviceImpl) {
        this.serviceImpl = serviceImpl;
        return this;
    }

    public String getFeiginDto() {
		return feiginDto;
	}

	public void setFeiginDto(String feiginDto) {
		this.feiginDto = feiginDto;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getMapper() {
        return mapper;
    }

    public PackageConfig setMapper(String mapper) {
        this.mapper = mapper;
        return this;
    }

    public String getXml() {
        return xml;
    }

    public PackageConfig setXml(String xml) {
        this.xml = xml;
        return this;
    }

    public String getRestController() {
		return restController;
	}

	public void setRestController(String restController) {
		this.restController = restController;
	}

	public String getRepository() {
		return repository;
	}

	public void setRepository(String repository) {
		this.repository = repository;
	}

	public String getFeigin() {
		return feigin;
	}

	public void setFeigin(String feigin) {
		this.feigin = feigin;
	}

	public String getController() {
        if (StringUtils.isEmpty(controller)) {
            return "web";
        }
        return controller;
    }

    public PackageConfig setController(String controller) {
        this.controller = controller;
        return this;
    }

	public String getBoot() {
		return boot;
	}

	public void setBoot(String boot) {
		this.boot = boot;
	}

}
