package com.alinesno.cloud.base.notice.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2019-06-08 07:19:08
 */
@SuppressWarnings("serial")
public class SmsSendDto extends BaseDto {

    /**
     * 业务关键字
     */
	private String bizId;
	
    /**
     * 业务主键
     */
	private String businessId;
	
    /**
     * 模板内容
     */
	private String content;
	
    /**
     * 其它属性
     */
	private String outId;
	
    /**
     * 手机号
     */
	private String phone;
	
	private String signName;
	
    /**
     * 模板内容
     */
	private String template;
	
    /**
     * 模板代码
     */
	private String templateCode;
	
    /**
     * 验证码
     */
	private String validateCode;
	


	public String getBizId() {
		return bizId;
	}

	public void setBizId(String bizId) {
		this.bizId = bizId;
	}

	public String getBusinessId() {
		return businessId;
	}

	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getOutId() {
		return outId;
	}

	public void setOutId(String outId) {
		this.outId = outId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSignName() {
		return signName;
	}

	public void setSignName(String signName) {
		this.signName = signName;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	public String getValidateCode() {
		return validateCode;
	}

	public void setValidateCode(String validateCode) {
		this.validateCode = validateCode;
	}

}
