package com.alinesno.cloud.base.storage.rest;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alinesno.cloud.base.storage.entity.StorageFileEntity;
import com.alinesno.cloud.base.storage.enums.StrategyEnums;
import com.alinesno.cloud.base.storage.service.IStorageFileService;
import com.alinesno.cloud.base.storage.service.StorageService;
import com.alinesno.cloud.base.storage.utils.RestfulUploadUtils;
import com.alinesno.cloud.common.core.context.ApplicationContextProvider;
import com.alinesno.cloud.common.core.rest.BaseRestController;

import feign.codec.Encoder;
import feign.form.spring.SpringFormEncoder;

/**
 * <p> 接口 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-09 20:22:17
 */
@Scope("prototype")
@RestController
@RequestMapping("storageFile")
public class StorageFileRestController extends BaseRestController<StorageFileEntity , IStorageFileService> {

	//日志记录
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(StorageFileRestController.class);

	@Autowired
	private RestfulUploadUtils restfulUploadUtils ; 

	@Configuration
    class MultipartSupportConfig {
        @Bean
        public Encoder feignFormEncoder() {
            return new SpringFormEncoder();
        }
    }
	
	/**
	 * 文件上传
	 * @param fileLoalAbcPath
	 * @param strategy
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@PostMapping("/uploadFile")
	public StorageFileEntity uploadFile(@RequestParam("file") MultipartFile file,  
			String username, 
			String strategy) throws Exception {
		
		strategy = StringUtils.isBlank(strategy)?StrategyEnums.LOCAL.value():strategy ;
		
		String fileName = file.getOriginalFilename();
		FileUtils.copyInputStreamToFile(file.getInputStream(), new File("/Users/luodong/Desktop/aaa_"+fileName));
		
		String fileLoalAbcPath = restfulUploadUtils.tmpFile(file , fileName) ; 
		StorageService storageService = (StorageService) ApplicationContextProvider.getBean(strategy+"StorageService") ; 
		
		return storageService.uploadData(fileLoalAbcPath , fileName) ; 
	}

	/**
	 * 获取文件下载链接
	 * @param fileId 文件保存id
	 * @return
	 */
	public String downloadData(String fileId) {
		
		return null ; 
	}

	/**
	 * 删除文件
	 * @param fileId
	 * @return
	 */
	public boolean deleteData(String fileId) {
		return false ; 
	}
	
}
