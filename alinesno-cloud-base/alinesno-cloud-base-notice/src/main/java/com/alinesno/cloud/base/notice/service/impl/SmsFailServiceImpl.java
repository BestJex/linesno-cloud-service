package com.alinesno.cloud.base.notice.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.base.notice.entity.SmsFailEntity;
import com.alinesno.cloud.base.notice.repository.SmsFailRepository;
import com.alinesno.cloud.base.notice.service.ISmsFailService;
import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 16:08:00
 */
@Service
public class SmsFailServiceImpl extends IBaseServiceImpl<SmsFailRepository, SmsFailEntity, String> implements ISmsFailService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(SmsFailServiceImpl.class);

}
