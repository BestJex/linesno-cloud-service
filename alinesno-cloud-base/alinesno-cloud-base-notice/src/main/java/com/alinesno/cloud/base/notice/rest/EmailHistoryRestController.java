package com.alinesno.cloud.base.notice.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;

import com.alinesno.cloud.base.notice.entity.EmailHistoryEntity;
import com.alinesno.cloud.base.notice.service.IEmailHistoryService;
import org.springframework.web.bind.annotation.RestController;
import com.alinesno.cloud.common.core.rest.BaseRestController;

/**
 * <p> 接口 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-08 06:27:10
 */
@Scope(SpringInstanceScope.PROTOTYPE)
@RestController
@RequestMapping("emailHistory")
public class EmailHistoryRestController extends BaseRestController<EmailHistoryEntity , IEmailHistoryService> {

	//日志记录
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(EmailHistoryRestController.class);

}
