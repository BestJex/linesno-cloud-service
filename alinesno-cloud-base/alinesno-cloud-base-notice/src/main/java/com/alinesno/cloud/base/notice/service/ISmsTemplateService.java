package com.alinesno.cloud.base.notice.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.notice.entity.SmsTemplateEntity;
import com.alinesno.cloud.base.notice.repository.SmsTemplateRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 16:08:00
 */
@NoRepositoryBean
public interface ISmsTemplateService extends IBaseService<SmsTemplateRepository, SmsTemplateEntity, String> {

}
