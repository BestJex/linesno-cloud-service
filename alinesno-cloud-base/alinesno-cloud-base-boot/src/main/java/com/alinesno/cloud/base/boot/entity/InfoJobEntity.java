package com.alinesno.cloud.base.boot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Entity
@Table(name="info_job")
public class InfoJobEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

	private String owners;
	@Column(name="job_type")
	private String jobType;
	@Column(name="job_name")
	private String jobName;
	@Column(name="job_parent")
	private String jobParent;


	public String getOwners() {
		return owners;
	}

	public void setOwners(String owners) {
		this.owners = owners;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobParent() {
		return jobParent;
	}

	public void setJobParent(String jobParent) {
		this.jobParent = jobParent;
	}


	@Override
	public String toString() {
		return "InfoJobEntity{" +
			"owners=" + owners +
			", jobType=" + jobType +
			", jobName=" + jobName +
			", jobParent=" + jobParent +
			"}";
	}
}
