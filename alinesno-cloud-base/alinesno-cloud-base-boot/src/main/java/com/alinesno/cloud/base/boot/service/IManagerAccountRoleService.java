package com.alinesno.cloud.base.boot.service;

import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.boot.entity.ManagerAccountRoleEntity;
import com.alinesno.cloud.base.boot.repository.ManagerAccountRoleRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-08 08:14:52
 */
@NoRepositoryBean
public interface IManagerAccountRoleService extends IBaseService<ManagerAccountRoleRepository, ManagerAccountRoleEntity, String> {

	List<ManagerAccountRoleEntity> findAllByAccountId(String accountId);

	void deleteByAccountId(String id);

}
