package com.alinesno.cloud.base.boot.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alinesno.cloud.base.boot.entity.ManagerAccountEntity;
import com.alinesno.cloud.base.boot.service.IManagerAccountService;
import com.alinesno.cloud.common.core.rest.BaseRestController;

/**
 * <p> 接口 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Scope("prototype")
@RestController
@RequestMapping("managerAccount")
public class ManagerAccountRestController extends BaseRestController<ManagerAccountEntity , IManagerAccountService> {

	//日志记录
	private final static Logger log = LoggerFactory.getLogger(ManagerAccountRestController.class);

	// 根据登陆用户名获取密码
	@PostMapping("findByLoginName")
	ManagerAccountEntity findByLoginName(String loginName) {
		log.debug("login name = {}" , loginName);
		ManagerAccountEntity e = feign.findByLoginName(loginName); 
		return e ; 
	}

}
