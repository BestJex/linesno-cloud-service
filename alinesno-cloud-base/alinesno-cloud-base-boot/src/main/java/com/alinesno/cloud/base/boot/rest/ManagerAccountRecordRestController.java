package com.alinesno.cloud.base.boot.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alinesno.cloud.base.boot.entity.ManagerAccountRecordEntity;
import com.alinesno.cloud.base.boot.service.IManagerAccountRecordService;
import com.alinesno.cloud.common.core.rest.BaseRestController;

/**
 * <p> 接口 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-08 22:18:49
 */
@Scope("prototype")
@RestController
@RequestMapping("managerAccountRecord")
public class ManagerAccountRecordRestController extends BaseRestController<ManagerAccountRecordEntity , IManagerAccountRecordService> {

	//日志记录
	@SuppressWarnings("unused")
	private final static Logger logger = LoggerFactory.getLogger(ManagerAccountRecordRestController.class);

}
