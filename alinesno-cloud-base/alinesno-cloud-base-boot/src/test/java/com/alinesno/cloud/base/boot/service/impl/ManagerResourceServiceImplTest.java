package com.alinesno.cloud.base.boot.service.impl;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.base.boot.entity.ManagerResourceEntity;
import com.alinesno.cloud.base.boot.service.IManagerResourceService;
import com.alinesno.cloud.common.core.junit.JUnitBase;

/**
 * 菜单单元测试类
 * @author LuoAnDong
 * @since 2019年1月20日 上午10:48:28
 */
public class ManagerResourceServiceImplTest extends JUnitBase {
	
	protected final Logger log = LoggerFactory.getLogger(this.getClass()) ; 

	@Autowired
	private IManagerResourceService managerResourceService ; 
	
	@Test
	public void testFindMenus() {
		String resourceParent = "536478251871109120" ; 
		String applicationId = "566970396281143296" ; 
		String accountId = "567263477580693504" ; 
		
		ManagerResourceEntity subResource = managerResourceService.findMenus(resourceParent , applicationId , accountId) ; 
		for(ManagerResourceEntity r : subResource.getSubResource()) {
			log.debug("r:{}" , ToStringBuilder.reflectionToString(r));
		}
	}

}
