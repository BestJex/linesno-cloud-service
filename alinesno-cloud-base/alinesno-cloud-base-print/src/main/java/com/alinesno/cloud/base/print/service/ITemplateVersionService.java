package com.alinesno.cloud.base.print.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.print.entity.TemplateVersionEntity;
import com.alinesno.cloud.base.print.repository.TemplateVersionRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-05-03 14:07:38
 */
@NoRepositoryBean
public interface ITemplateVersionService extends IBaseService<TemplateVersionRepository, TemplateVersionEntity, String> {

}
